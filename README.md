# Convenia

## Instalando as dependências
Para instalar as dependências, rode o comando: 
`yarn`

## Executando o projeto
Para executar o projeto, rode o comando:
`yarn serve`

## Abrindo o projeto
Para abrir o projeto, realize sua execução e após isso, abra seu navegador e digite `localhost:8080` na url.