export const fetchQuoteCurrency = async () => {
  const BASE_URL = 'https://economia.awesomeapi.com.br/last/USD-BRL,EUR-BRL'
  const response = await fetch(BASE_URL);
  return await response.json();
}